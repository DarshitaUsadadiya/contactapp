package example.contactsapp;

import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Pattern;

/**
 * Created by Darshita on 18/2/18.
 */

public class EditContact extends AppCompatActivity {
    private EditText mname;
    private EditText mnumber;

    @Override

    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_edit);
        setTitle("Edit Contact");
        Bundle bundle = null;
        bundle = this.getIntent().getExtras();
        final int id1 = bundle.getInt("id");
        final String name = bundle.getString("name");
        String number = bundle.getString("number");

        mname = (EditText) findViewById(R.id.name);
        mnumber = (EditText) findViewById(R.id.number);
        mname.setText(name);
        mnumber.setText(number);

            Button button = (Button) findViewById(R.id.button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String vname=mname.getText().toString();
                    String vnumber=mnumber.getText().toString();
                    if (vname != null && !vname.isEmpty() && !vnumber.isEmpty()){
                    ContentValues values = new ContentValues();
                    values.put(ContactProvider.NAME, vname);
                    values.put(ContactProvider.NUMBER, vnumber);
                    int i = getContentResolver().update(ContactProvider.CONTENT_URI, values, "_id=" + id1, null);
                    Toast.makeText(EditContact.this,"contact Edited succesfully",Toast.LENGTH_LONG).show();

                }
                else
                    {
                        Toast.makeText(EditContact.this,"Enter Name and mobile number",Toast.LENGTH_LONG).show();
                    }

                }
            });
            Button delete = (Button) findViewById(R.id.delete);

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ContentValues values = new ContentValues();
                    values.put(ContactProvider.NAME, mname.getText().toString());
                    values.put(ContactProvider.NUMBER, mnumber.getText().toString());
                    int i = getContentResolver().delete(ContactProvider.CONTENT_URI, "_id=" + id1, null);
                    Toast.makeText(EditContact.this,"contact deleted succesfully",Toast.LENGTH_LONG).show();


                }
            });
        }
    }



