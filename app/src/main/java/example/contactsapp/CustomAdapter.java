package example.contactsapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Darshita on 18/2/18.
 */

public class CustomAdapter extends BaseAdapter{

    private ArrayList<ListModel> data;
    int i = 0;


    public CustomAdapter( ArrayList<ListModel> d) {
        data = d;
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {

        public TextView name;
    }

     public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inf = LayoutInflater.from(parent.getContext());

            vi = inf.inflate(R.layout.contact_row, null);
            holder = new ViewHolder();
            holder.name = (TextView) vi.findViewById(R.id.name);

            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }

        ListModel  tempValues = (ListModel) data.get(position);
        holder.name.setText(tempValues.getName());
        return vi;
    }


}