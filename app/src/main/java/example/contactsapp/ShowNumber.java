package example.contactsapp;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Darshita on 18/2/18.
 */

public class ShowNumber extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_contact);
        setTitle("Contact");
        Bundle bundle=null;
        bundle=this.getIntent().getExtras();
        final int id1=bundle.getInt("bundle");
        final String name1=bundle.getString("name");
        final String number1=bundle.getString("number");

        TextView mname=(TextView) findViewById(R.id.name);
        TextView mnumber=(TextView) findViewById(R.id.number);
        mname.setText(name1);
        mnumber.setText(number1);

        ImageView imgview=(ImageView) findViewById(R.id.edit);
        imgview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle=new Bundle();
                bundle.putInt("id",id1);
                bundle.putString("name",name1);
                bundle.putString("number",number1);
                Intent intent = new Intent(ShowNumber.this, EditContact.class);
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });
    }
}
