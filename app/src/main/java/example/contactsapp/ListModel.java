package example.contactsapp;

import java.io.Serializable;

/**
 * Created by Darshita on 18/2/18.
 */

public class ListModel implements Serializable {
    private  int ID;
    private  String name="";
    private  String number="";


    public void setID(int ID) {this.ID = ID;}
    public void setName(String name)
    {
        this.name = name;
    }
    public void setNumber(String number) {this.number = number;}



    public int getID()
    {
        return this.ID;
    }
    public String getName()
    {
        return this.name;
    }
    public String getNumber()
    {
        return this.number;
    }


}

