package example.contactsapp;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static android.R.attr.onClick;

/**
 * Created by Darshita on 18/2/18.
 */

public class AddContact extends AppCompatActivity {
    EditText name;
    EditText number;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_contact);
        setTitle("Add Contact");
        button = (Button) findViewById(R.id.button);
        name =(EditText)findViewById(R.id.name);
        number =(EditText)findViewById(R.id.number);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String vname=name.getText().toString();
                String vnumber=number.getText().toString();
                if (vname != null && !vname.isEmpty() && !vnumber.isEmpty()) {
                    ContentValues values = new ContentValues();
                    values.put(ContactProvider.NAME, vname);
                    values.put(ContactProvider.NUMBER, vnumber);
                    Uri uri = getContentResolver().insert(ContactProvider.CONTENT_URI, values);
                    Toast.makeText(AddContact.this,"contact added succesfully",Toast.LENGTH_LONG).show();
                    finish();
                }
                else

                    {
                        Toast.makeText(AddContact.this,"Enter Name and mobile number",Toast.LENGTH_LONG).show();
                    }


            }
        });

    }

}
