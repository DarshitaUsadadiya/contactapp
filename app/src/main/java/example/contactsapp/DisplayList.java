package example.contactsapp;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by Darshita on 18/2/18.
 */

public class DisplayList extends AppCompatActivity {

    public ArrayList<ListModel> CustomListViewValuesArr;
    ListView list;
    CustomAdapter adapter;
    private FloatingActionButton button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_list);
        setTitle("Contacts");
        CustomListViewValuesArr = new ArrayList<>();


        list = (ListView) findViewById(R.id.list);

        adapter = new CustomAdapter(CustomListViewValuesArr);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                ListModel model = CustomListViewValuesArr.get(position);
                Bundle bundle = new Bundle();
                bundle.putInt("bundle", model.getID());
                bundle.putString("name", model.getName());
                bundle.putString("number", model.getNumber());
                Intent i = new Intent(DisplayList.this, ShowNumber.class);
                i.putExtras(bundle);
                startActivity(i);
            }

        });
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.btn);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DisplayList.this, AddContact.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setListData();
    }

    public void setListData() {
        CustomListViewValuesArr.clear();
        Cursor c = getContentResolver().query(ContactProvider.CONTENT_URI, null, null, null, ContactProvider.NAME);


        if (c.moveToFirst()) {
            do {

                final ListModel sched = new ListModel();
                sched.setID(c.getInt(c.getColumnIndex(ContactProvider._ID)));
                sched.setName(c.getString(c.getColumnIndex(ContactProvider.NAME)));
                sched.setNumber(c.getString(c.getColumnIndex(ContactProvider.NUMBER)));

                Log.d("GetCountValue", c.getString(c.getColumnIndex(ContactProvider._ID)));
                CustomListViewValuesArr.add(sched);

            } while (c.moveToNext());
        }
        c.close();


    }


}
